%  Usage: [i,istr]=mpsIsHConj(A [,eps]);
%
%  check whether QSpace is hermitian within threshold eps (1E-12).
%  A must be even-rank object as hyperindex is allowed.
%
% Wb,Aug17,06
