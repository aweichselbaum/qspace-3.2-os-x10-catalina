function r=if3(cond, val1, val2)
% Function r=if3(cond, val1, val2)
% Wb,Jan17,08

   if cond, r=val1; else r=val2; end

end

