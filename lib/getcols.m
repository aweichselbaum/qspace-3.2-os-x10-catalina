function ncols=getcols()
% function ncols=getcols()
% Wb,Nov11,11

% NB! after repeated calls, this may take 10 sec for each call !(*Y_!
% Wb,May02,13
  persistent nc
  if isempty(nc)
     e=system('which getcols.pl >/dev/null');
     if e, nc=80;
        wblog('WRN','getcols.pl does not exist (using ncol=%g)',nc);
     else
        nc=system('getcols.pl -q 2>/dev/null');
        if isempty(nc) || nc<64, nc(2)=80;
           wblog('WRN','got ncols=%g -> %g',nc);
           nc=nc(2);
        end
     end

  end

  ncols=nc;

end

