function [str,e]=rat2(x,varargin)
% function str=rat2(x [,fmt])
%
%    Get fractional representation of x, if any
%    (based on MatLab's rat.m)
%
%    The format specifier fmt (default: %g) is used only
%    if no sensible fractional representation can be found.
%
% Options
%
%    '-u',{uval,ustr}  specificy units
%    '-s'              return string (rather than cell array of strings)
%    '-r'              use square root / return string 'sqrt(p/q)'
%  
% See also rats()
% Wb,Mar31,05

  if ~nargin
     helpthis, if nargin || nargout, wberr('invalid usage'), end
     return
  end

  getopt('init',varargin);
     udat =getopt('-u',{});
     sflag=getopt('-s');
     rflag=getopt('-r');
  fmt=getopt('get_last','%g');

  nx=numel(x); if ~nx, str=''; e=[]; return; end

  if isempty(udat) && rflag && sflag
     str=cell(1,nx);
     for i=1:nx
        str{i}=regexprep(rats(x(i)^2),' ','');
     end
     if nx==1
        str=['sqrt(' str{1} ')'];
        if x<0 str=['-' str]; end
     elseif all(x(:)>=0)
        str=[', ' str{:}];
        str=['sqrt([' str(3:end) '])'];
     else wberr('got negative values with -r flag'); end

     if nargout>1
        eval(['q=' sx ';']); q=reshape(q,size(x));
        a=abs(x); a(find(a==0))=1;
        e=abs(q-x)/a;
     end
     return
  end

  if ~isempty(udat)
     if ~iscell(udat) || numel(udat)~=2 || numel(udata{1})~=1
     wberr('invalid usage: ''-u'',{uval,ustr}'); end
     if udat{1}>0, x=x/udat{1};
     else
        wblog('WRN','ignorig unit %g',udat{1}); 
        udat={};
     end
  end

  if rflag
       [P,Q]=rat(x^2);
  else [P,Q]=rat(x);
  end

  str=cell(1,nx);
  for i=1:nx, p=P(i); q=Q(i);
     if abs(p)<32 && abs(q)<32
        if p~=0
           if isempty(udat)
              if q~=1, str{i}=sprintf('%d/%d',p,q);
              else str{i}=sprintf('%d',p); end
           else 
              if p==1
                 if q~=1, str{i}=sprintf('%s/%d',udat{2},q);
                 else str{i}=sprintf('%s',udat{2}); end
              elseif p==-1
                 if q~=1, str{i}=sprintf('-%s/%d',udat{2},q);
                 else str{i}=sprintf('-%s',udat{2}); end
              else
                 if q~=1, str{i}=sprintf('(%d/%d)%s',p,q,udat{2});
                 else str{i}=sprintf('%d%s',p,udat{2}); end
              end
           end
        else str{i}='0'; end
     else 
        if isempty(udat), str{i}=sprintf(fmt,x(i));
        else str{i}=[ sprintf(fmt,x(i)) udat{2}]; end
     end

     if rflag
        str{i}=sprintf('sqrt(%s)',str{i});
        if sflag && x(i)<0, str{i}=['-' str{i}]; end
     end
     if i>1 && x(i)>=0 && sflag
        str{i}=[' + ' str{i}];
     end
  end

  if sflag, str=[str{:}];
     if nargout>1
        eval(['q=' str ';']); q=reshape(q,size(x));
        e=abs(q-x)/abs(x);
     end
  else
     if nx==1, str=str{1};
     if nargout>1
        q=P./Q; q=reshape(q,size(x));
        e=abs(q-x)/abs(x);
     end
  end

end

