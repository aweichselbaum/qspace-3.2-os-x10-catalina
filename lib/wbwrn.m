function wbwrn(varargin)
% function wbwrn(varargin)
% Wb,Jun01,19

% adapted from wberr.m // Wb,Jun01,19

   if nargin>1,   s=sprintf(varargin{:});
   elseif nargin, s=varargin{1};
   else s=''; end

   s=regexprep([10 s],'\n','\n   WRN ');
   s=regexprep(s,'\\n','\n   WRN ');

   ib=~isbatch();
   if ib, printfc('\e[35m');, end
          fprintf(1,s);
   if ib, printfc('\e[0m\n\n'); else fprintf(1,'\n\n'); end

   S=dbstack;
   if numel(S)>1, dispstack(S(2:end)); end

   warning('Wb:WRN','');

end

