function wberr(varargin)
% function wberr(varargin)
% see also wbdie.m
% Wb,May18,07 ; Wb,Jun01,19

   if nargin, s=regexprep(varargin{1},'\\N',char(11));
      if nargin>1
         s=sprintf(s,varargin{2:end});
         s=regexprep(s,'\\N',char(11));
      end
   else s=''; end

   s=regexprep([10 s],'\n','\n   ERR ');
   s=regexprep(s,'\\n','\n   ERR ');
   s=regexprep(s,char(11),[char(10) '   ']);

  q=beep;
  if isequal(q,'off'), beep on; beep; beep off; end

   ib=~isbatch();
   if ib, printfc('\e[31m');, end
          fprintf(1,s);
   if ib, printfc('\e[0m\n\n'); else fprintf(1,'\n\n'); end

   S=dbstack;
   if numel(S)>1, dispstack(S(2:end)); end

  S=struct('message','','identifier','Wb:ERR', 'stack',S(min(2,end)));
  error(S);

end

