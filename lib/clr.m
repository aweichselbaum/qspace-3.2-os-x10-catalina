function clr(varargin)
% function clr([opts])
 %
%    clear entire workspace (and terminal window).
%
% Options
%
%   -k  keep figures, classes, debug stops
%
% Wb,Jan05,07

  try
  getopt ('init', varargin);
     keep = getopt ('-k');
     figf = getopt ('-f');
     eflag= getopt ('-e');  % call subsequent 'dbstop if error'
     rflag= getopt ('-r');
  varargin=getopt('check_error');
  catch
     return
  end

% ----------------------------------------------------------------------- %
  if ~keep
     if ~figf
     evalin('caller','close all'); else untagf -a -q; end
     evalin('caller','clear functions; dbclear all; clear classes');
  end

  evalin('caller','clear all; clear global');

  if eflag, dbstop if error; end
  if rflag, system('reset'); end

  if keep || figf, untagf -a -q; return; end
  if rflag, return; end

  L=str2num(getenv('LINES'));
  if ~isempty(L), if L<80, L=80; end
  else
     L=evalc('!tput cols 2>&1');
     if ~isempty(L), L=str2num(L); end
     if isempty(L), L=80; end
  end

  s='   C L E A R   A L L';

  l=[0 length(s)]; l(3)=L-sum(l);
  s=[ '\n\e[48;5;15;38;5;0m' repmat(' ',1,l(1)), s, ...
      repmat(' ',1,l(3)) '\e[0m\n' ...
      '\e[38;5;0m>>\e[0m\n' % print '>>' black, and hence invisible ^1)
  ];

  printfc(s);
  clc; return

  L=getenv('LINES'); if ~isempty(L), L=str2num(L); else L=30; end

  p={'clr_line.txt',  0};
% p={'clr_eagle.txt',14};
% p={'clr_aux.txt',  14};
% p={'',             -2};
% p={'clr_aux0.txt',  4};
% p={'clr_auxx.txt',  8};

  nl=10; n=ceil((L-p{2})/2)-4;

  disp([10 '%% CLEAR ALL ' repmat('%',1,73) 10 10]);

  disp(strvcat(' ', nl(ones(1,n))))

  if ~isempty(p{1}), type(p{1}); end

  disp(strvcat(' ', nl(ones(1,n))))

  clear; clc

end

