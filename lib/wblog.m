function rval=wblog(varargin)
% function wblog(fmt,...)
 %
%    logging routine with output:
%    filename:linenr (output as with sprintf())
%
% format string
%
%    \n  leads to newline with usual file:line header
%    \N  leads to old fashioned newline, i.e. with no header
%
%    except for the very first appearances of \N or \n in fmt,
%    which are interpreted as global leading \n
%
% Wb,Nov18,05: allow to redirect output to file
%
%    wblog('setfid',fid)     - redirects the following wblog's to file fid
%    wblog('setfout',fname)  - redirects the following wblog's to file fname
%    wblog('unsetfid')
%    wblog('unsetfout')      - unset the output to file
%
% Based on lineno() from C.Denham which uses dbstack()
% for more info: see help to lineno()
%
% Wb,Oct08,03

  persistent fid newfile llday

  if nargin==0, eval(['help ' mfilename]); return; end
  if nargin==1

     if isequal(varargin{1},'-ping')
        llday=check_nextday(llday,fid); return
     elseif ~isempty(regexp(varargin{1},'^--got-')), q=varargin{1}(7:end);
        u=getuser(0,'err_count'); rval=0;
        if     regexpi(q,'^ERR$'), if isfield(u,'err'), rval=u.err; end
        elseif regexpi(q,'^WRN$'), if isfield(u,'wrn'), rval=u.wrn; end
        elseif regexpi(q,'^TST$'), if isfield(u,'tst'), rval=u.tst; end
        else wberr('invalid usage (%s)',q); end
        return
     elseif isequal(varargin{1},'unsetfid')
        fid=[]; return
     elseif isequal(varargin{1},'unsetfout')
        if ~newfile
           wblog('ERR','fid set by calling routine.');
        elseif ~isempty(fid)
           fclose(fid);
        else wblog('ERR','No file to close.'); end

        fid=[]; return
     end

  elseif nargin==2

     if isequal(varargin{1},'setfid')

        if ~isempty(fid)
        wblog('ERR','must close active fid first.'); return; end

        fid=varargin{2}; newfile=0;
        try, fprintf(fid,'');
        catch,fid=[]; wblog('ERR','Invalid fid.'); end
        return

     elseif isequal(varargin{1},'setfout')

        if ~isempty(fid)
        wblog('ERR','must close active fid first.'); return; end

        fname=varargin{2};
        fid=fopen(fname,'w'); newfile=1;
        try, fprintf(fid,'');
        catch,fid=[]; wblog('ERR','Cannot write to file %s.',fname); end
        return

     end
  end

  if ~ischar(varargin{1}) && length(varargin{1})==1
       idx=varargin{1}; varargin(1)=[];
  else idx=0; end

  s=lineno_aux(2+idx);
  t=datestr(now);

  tag=''; TAG=''; nargs=numel(varargin);
  if nargs>1 && ischar(varargin{2}) && isempty(find(varargin{1}=='%'))
     tag=varargin{1}; varargin(1)=[];
  elseif nargs
     [i,j,x]=regexp(varargin{1},'^\s*(.)\s+');
     if ~isempty(i) && x{1}(1)<=3 && j(1)<5
        TAG=sprintf('LL%d',x{1}(1)); tag=varargin{1}(1:3);
        varargin{1}=varargin{1}(5:end);
     else
        l=regexp(varargin{1},'\s');
        if ~isempty(l) && l(1)<8, l=l(1); q=varargin{1}(1:l-1);
           if isempty(findstr(q,'%'))
              [i,j,x]=regexp(q,'^(WRN|ERR|LL\d|NB!|CST|Error|Warning)');
              if ~isempty(i), x=x{1};
                 tag=q(x(1):x(2));
                 varargin{1}=varargin{1}(l+1:end);
              end
           end
        end
     end
  end

  cstflag=0; if isempty(TAG), TAG=tag; end

  if ~isempty(tag)
     switch tag
       case {'LL1'}, tag='*  ';
       case {'LL2'}, tag=' * ';
       case {'LL3'}, tag='  *';
       case {'CST'}, cstflag=1; tag='';
       case {'Error'}, tag='ERR';
       case {'Warning'}, tag='WRN';
     end
     if ~cstflag, tag=sprintf('%3s ',tag); end

     if ~isempty(tag)
        u=get(0,'UserData'); f=deblank(lower(tag));
        if isempty(u) || ~isfield(u,'err_count')
        set_global; u=get(0,'UserData'); end

        g=u.err_count;
        if isfield(g,f)
           g=setfield(g,f, getfield(g,f)+1);
           u.err_count=g;
           set(0,'UserData',u);
        end
     end
  end

  wesc={0};
  if isempty(fid) && exist('printfc')==3 && ~isbatch()
     if     ~isempty(regexp (tag,'WRN')), wesc={1,'35'};
     elseif ~isempty(regexp (tag,'ERR')), wesc={2,'31'};
     elseif ~isempty(regexp (tag,'NB!')), wesc={4,'32'};
     elseif ~isempty(regexp (tag,'ok[\.!]')) || ...
            ~isempty(regexp (tag,'=+>+'   )) || ...
            ~isempty(regexpi(tag,'SUC'    )), wesc={4,'38;5;28'};
     end
     if wesc{1}, printfc(['\e[' wesc{2} 'm']); end
  end
  werr=bitand(wesc{1},3);

  hstr=sprintf('%-20.20s %s  %s', shortfstr(s,20), t(13:end), tag);

  fstr=hstr; % fstr(:)=' '; % fstr(end-1)='.';

  fmt = varargin{1};

  for i=1:length(fmt), if fmt(i)~=10, break, end, end
  if i>1, inl(i-1); fmt=fmt(i:end); end

  while length(fmt)>2 && (isequal(fmt(1:2),'\N') || isequal(fmt(1:2),'\n'))
     fprintf(1,'\n'); fmt=fmt(3:end);
  end

  nl=char(10);

  fmt = strrep(fmt,char(10),[nl fstr]);
  fmt = strrep(fmt, '\n', [nl fstr]);
  fmt = strrep(fmt, '\N', nl);

  if     length(fmt)>1 && isequal(fmt(1:2),'\\'), fmt=fmt(3:end);
  elseif length(fmt)>1 && isequal(fmt(1:2),'\r'), fmt=['\r' hstr fmt(3:end)];
  else fmt=[hstr fmt]; end

  if isequal(fmt(max(1,end-1):end),'\\'), fmt=fmt(1:end-2);
  else fmt=[fmt nl]; end

  llday=check_nextday(llday,fid);

  if isempty(fid)
     fprintf(1,fmt,varargin{2:end});
     if wesc{1}, printfc('\e[0m'); end
  else
     fprintf(fid,fmt,varargin{2:end});
  end

  if ~werr, return; end

% -------------------------------------------------------------------- %
  q=werr;
  if q
     if     (q&1) && ~isset(getuser(0,'dbstop_if_WRN')), q=0;
     elseif (q&2) && ~isset(getuser(0,'dbstop_if_ERR')), q=0; end
  end
  if ~q
     if ~isempty(who('global','DEBUG')), global DEBUG
     else
        DEBUG=getenv('DEBUG'); if isempty(DEBUG)
        DEBUG=getenv('ML_DEBUG'); end
     end
     if ~isempty(DEBUG), q=werr; end
  end

  if ~q, return; end

  S=dbstatus; if isempty(S), return; end
  q_=0; for i=1:numel(S)
     if     isequal(S(i).cond,'warning'), q_=bitor(q_,1);
     elseif isequal(S(i).cond,'error'  ), q_=bitor(q_,2); end
  end

  if bitand(q,q_)
   % use regular keyboard stop, so code can be resumed (e.g. after WRN)
   % then use e.g. dbquit to *not* resume program // Wb,May31,19
   % if bitand(q,2), inl(1); dbstack; inl(1); keyboard % beep; 
   % elseif q,       inl(2); dbstack; inl(1); keyboard % beep; 
   % end
     wbstop
  else
     if bitand(q,2) inl(2); error('Wb:ERR',''); end
  end
end

% -------------------------------------------------------------------- %
function line = lineno_aux(id)

  [stack,index]=dbstack;
  stack=stack(min([1+id, length(stack)]));

  if ~isequal(stack.file, [stack.name '.m']);
       line = sprintf('%s>%s:%d', stack.file, stack.name, stack.line);
  else line = sprintf('%s:%d', stack.file, stack.line); end

end

% -------------------------------------------------------------------- %
function llday=check_nextday(llday,fid)

  d=datevec(now); d=d(3);
  if ~isequal(d,llday)
     if ~isempty(llday)
        if isempty(fid), f=1; else f=fid; end
        fprintf(f,'\n\n>> TODAY %s :: (%d->%d)',datestr(now),llday,d);
        fprintf(f,'\n\n');
     end
     llday=d;
  end

end

% -------------------------------------------------------------------- %
