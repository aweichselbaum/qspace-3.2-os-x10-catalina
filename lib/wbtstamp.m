function s=wbtstamp(varargin)
% deprecated: use wbstamp instead

% s=['Wb' datestr(now,'yymmdd')];
  s=wbstamp(varargin{:});

end

