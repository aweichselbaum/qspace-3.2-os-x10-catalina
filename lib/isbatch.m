function i=isbatch
% Function: isbatch
%
%    Check whether batchmode is set in get(0,'UserData').
%
% Wb,Sep06,07

  i=isdeployed();
  if i, return; end

  u=get(0,'UserData'); i=0;

  if isempty(u) || ~isfield(u,'batch')
     if ~isempty(getenv('SGE_O_HOST')) ...
     && ~isempty(getenv('SGE_O_HOME')) i=1; end
     return
  end

  if ~isscalar(u.batch)
     wblog('ERR','Invalid batchflag in UserData !??');
     disp(u); return
  end

  i=u.batch;

end

