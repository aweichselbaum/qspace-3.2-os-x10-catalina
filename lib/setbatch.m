function setbatch(bval)
% Function: setbatch
%
%    Set batchmode to set(0,'UserData').
%    To unset batch mode, use setbatch(0).
%
% Wb,Sep06,07

  u=get(0,'UserData');

  if nargin==0, bval=1; end

  if isempty(u)
  s.batch=bval; set(0,'UserData',s); return; end

  if ~isstruct(u)
     wblog('ERR','get(0,''UserData'' must be structure!');
     disp(u); return
  end

  u.batch=bval; set(0,'UserData',u);

end

