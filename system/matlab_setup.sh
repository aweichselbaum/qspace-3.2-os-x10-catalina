#!/bin/bash

# minimal centralized startup script to setup Matlab environment

  if ! `type module >/dev/null 2>&1`; then
     printf "\n  ERR $0 : module environment not available\n";
     exit 1
  fi

# realpath (in case that the linux comman realpath is not available)
# P=$(realpath "${BASH_SOURCE[0]}")
  P=$(perl -we "use Cwd 'realpath'; print realpath('${BASH_SOURCE[0]}')");
  P=`dirname "$P"`;

# -------------------------------------------------------------------- #
# NB! matlab requires very specific gcc version for mex/mcc compilation

  if [ -z $MATLAB_ROOT ]; then
   # my current default (@ LMU, Munich)
   # module load matlab/2016a # NB! matlab/R2016 pairs with gcc/4.7.4
   # module load gcc/4.7.4

     module load matlab/2018b # NB! matlab/R2018 pairs with gcc/6.3.x
     module load gcc/6.3.0
  fi

  if [ -z "$MATLAB_ROOT" ]; then
     MATLAB_ROOT=`which matlab`;
     export MATLAB_ROOT=${MATLAB_ROOT//bin*matlab*/}
  fi

# -------------------------------------------------------------------- #

  if [[ $USER =~ [Ww]eichselbaum ]]; then
   # feel free to remove this part (for debugging only)
   # export MYMATLAB=$HOME/Matlab/MPSPACK_v3.1
     export MYMATLAB="$P/.."
     export MSLOTS=12
  else
     export MYMATLAB=$HOME/Matlab  #> ml starts in here -> startup.m
  fi

  if [ ! $LMA ]; then
     export LMA=/data/$USER/Data
  fi
  if [ ! $RC_STORE ]; then
     export RC_STORE=$LMA/RCStore  # for non-abelian symmetries
     export RC_SYNC=$LMA/RCSync    # just for lock files
  fi
  
# may overwrite the defaults chosen by matlab_setup.pl below
# export QSP_NUM_THREADS=2  #> QSpace
# export OMP_NUM_THREADS=4  #> Intel MKL library
# export MKL_NUM_THREADS=4  #> Intel MKL library

# -------------------------------------------------------------------- #
# double check and complete parameter setting

  if [[ "$*" == '-t' ]]; then
       $P/matlab_setup.pl -t  # use this to check/test output
  else eval "$($P/matlab_setup.pl "$@")"
  fi

# -------------------------------------------------------------------- #
