
% demonstration of time-dependent DMRG for Dimitris Saraidaris
% based on the spin-half SU(2) Heisenberg model with nearest-
% neighbor exchange // Wb,Feb09,21

% first generate prototypical state (here ground state of
% Heisenberg Hamiltonian by running DMRG on it)
  if ~isvar('HAM') || isset('runDMRG')

     wsys='SU2-Heisenberg';
     setdef('Qtot',0); 
     NKEEP=2.^(3:5);

     setdef('L',32);
     J=rand(L-1,1);    oham={J};
     wblog(' * ',['using random NN Heisenberg couplings\n' ...
       'J=[%.3g .. %.3g]'], min(J),max(J)); 

     oham=[oham,{'qloc',1}];

     ofout={'fout',['./DMRG/' wsys sprintf('-L%g',L)]};

     [HAM]=Hamilton1D('Heisenberg',oham,ofout{:});

     L=length(HAM); Nkeep=NKEEP(1);

     onrg=setopts('--v',Nkeep,Qtot);

     [H0,Inrg]=initNRG(HAM,onrg{:});

     HAM

     setdef('rtol',1E-24');

     nsw=numel(NKEEP);
     HAM.info.sweep.nsw=nsw;
     HAM.info.sweep.rtol=rtol;

     if isbatch || isdeployed
          mat=sprintf('./%s-L%g',wsys,L);
     else mat=''; end

     for isw=1:nsw, Nkeep=NKEEP(isw);
        HAM.info.sweep.Nkeep=Nkeep;
        HAM.info.sweep.isw=isw;

      % ============================================================= %
        [r2,E0,Il,HAM,RR,EE]=sweepPsi(HAM);
        %  r2   discarded weight
        %  E0   [ ground state energy / site, variation during sweep ]
        %
        %  Il   Info structure *)
        %       .se  entanglement entropoy
        %       .rr  eigenvalues of reduced density matrix for given bond
        %       .r2  discarded weight at given step
        %       .Dk  dimensions kept (based on Nkeep and rtol)
        %       etc.
        %
        %  RR   reduced density matrices for bonds along the sweep *)
        %  EE   diagonalized block Hamiltonian for all bonds       *)
        %
        %  *)   two columns each (forward and backward sweep)
      % ============================================================= %

        e0=getdatafield(Il,'E0','-q'); i1=ceil(L/3); i2=L-i1+1;
        q=reshape(e0(i1:i2,:)',[],1);

        wblog(' * ','sweep %g/%g : <e0>=%s (@ %.3g)',...
          isw,nsw,num2str2(mean(q),std(q)),r2);

        if mat, save2(mat,'-f','-q'); end
     end
  end

  setdef('dt',0.025);

  Nkeep=128;

  setdef('nrep',5,'ntau',ceil(1/dt));

  osw=setopts(Nkeep,'stol?',ntau); % `sweep options'

  osw={'Nkeep',NKEEP(end),'ntau',5}; nrep=4;

% [HMX,It0]=setupTrotter2(HAM,dt,'ops',{HAM.ops(1).op,fix(L/2)});
%
%  * originally written for computing dynamical correlation functions;
%    hence an operator is applied in the system center at t=0
%    and after every full sweep this computes Sxt = S(x,t)
%    based on the operators 'ops' specified
%  * prepares starting state by applying spin operator [HAM.ops(1)]
%    onto the ground state in the system center (L/2)
%  * generates full MPO for unitary that propagates the state by 
%    time step dt based on 2nd order trotter
%  * in contrast to ground state sweeps above,
%    td-DMRG saves wave function in memory with HAM -> HMX.user.trotter
%
%     - HMX quickly becomes much larger than HAM
%       hence store separate from HAM
%
%     - after full time step is finished, saveTrotter2()
%       saves current wave function psi(t) to file (see HAM.mat)
%
%     - HMX is the same as HAM, except that it has HAM.user.trotter set
%       this is a structure with the content
%
%        .info   some internal info when setting up .mpo below
%        .mpo    full MPO for unitary U(dt)
%        .mat    (empty since all data is kept in memory)
%        .dt     time step used in .mpo above
%        .data   structure that stores wave function and temporary data
%
%       Fields in HAM.user.trotter.data (A generally stands for wave
%       function, like A-tensor in the underlying mps)
%
%        .A0     original wave function
%        .At     current wave function | psi(t) >
%        .Af     auxilliary space fitted wave function U(dt) | psi(t) >
%                which once done, set the next .At
%        .Xm     auxilliary space (block overlap matrices)
%
%  * use setupTrotter2(HAM,dt) if no opeator should be applied
%  * setupTrotter2(HAM,dt) loads [ HAM.mat '_info.mat' ]
%    from the ground state calculation above, since the Trotter
%    time step uses the Hamiltonian relative to the ground state
%    energy, i.e., U(dt) = exp(-i (H-E0) dt). This is a trivial
%    factor, e.g., required for dynamical correlation functions;
%    it is not that relevant here but should be kept anyway
%
% Recommended way to proceed:

  [HMX,It0]=setupTrotter2(HAM,dt);

  % 1) use the setupTrotter2() with the *ground state* calculation
  %    above to start with (because this also saves the ground state
  %    energy) etc.
  % 2) then replace HMX.user.trotter.data.At
  %    with the actual wave function you want to evolve
  %    with the orthogonality at site k=1 (i.e. right-to-left
  %    orthonormalized), and put a copy into HMX.user.trotter.data.Af
  % 3) keep ntau smaller if you wish to inspect the wave function
  %    [HMX.user.trotter.data.At] frequently in between time steps
  % 4) throughout, please double check whether this behaves
  %    as you expect it to (e.g. run test cases where you know
  %    what to get / you expect).

  for irep=1:nrep 
     [HMX,Iout(irep)]=sweepPsi_tmpo(HMX,osw{:});

     saveTrotter2(HAM,HMX,'-TR2');

     if mat, save2(mat,'-f','-x','HMX'); end
  end

