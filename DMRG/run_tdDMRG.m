function run_tdDMRG(varargin)
% function run_tdDMRG(opts)
% Wb,Jul01,19

% run_tdDMRG(...
%    'runTASK=''BB=linspace(0,1,6); B=BB(tid);''', ...
%    'runDMRG=''qloc=5; L=64; nk2=8''', ...
%    'runTDEP=''dt=0.01; ntau=100; nrep=20; Nkeep=512; ''')

%  qsub -t 2 ./pbsMCC.sh -F "run_tdDMRG $MATLAB_ROOT runTASK='BB=linspace(0,1,6); B=BB(tid)'  runDMRG='qloc=5; L=128; nk2=8' runTDEP='dt=0.01; ntau=100; nrep=20; Nkeep=512'"

  Imain.started=datestr(now);
  Imain.finished='';
  Imain.info=mlinfo;
  Imain.stamp=wbstamp;

  fprintf(1,'\n');
  startup_numthreads(Imain.info)
  startup_info(Imain.info)

  ! qsp_info.pl -k

  getopt('init',varargin);
     Imain.log=getopt('--log','');
  varargin=getopt('get_remaining');

  Imain.cmd={mfilename,varargin{:}};

  [jid,tid]=getJobID();

% NB! input arguments also may contain parameter script to run
% i.e. may not just include assignements
% i=find(cellfun(@numel,regexp(varargin,'=')));
% if ~isempty(i)
%    q=sprintf(' %s;',varargin{i}); fprintf(1,'\n>> args = %s\n',q);
%    eval(q); varargin(i)=[];
% end

  q=sprintf(' %s;',varargin{:}); fprintf(1,'\n>> %s\n',q(2:end));
  eval(q);

  if ~isempty(tid)
     if isset('runTASK') && ischar(runTASK)
        fprintf(1,'>> having job %g.%g\n',jid,tid);
        eval(runTASK)
     end
  end

  tst_tdDMRG

  fprintf(1,'\n  Started : %s\n',Imain.started);
  fprintf(1,  '  Finished: %s\n',datestr(now) );

end

