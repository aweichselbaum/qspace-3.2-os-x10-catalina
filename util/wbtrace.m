%  Usage: i = wbtrace(A, [i1 i2; i3 i4; ...] [,rank]);
%
%      generalized trace for arbitrary dimensional object
%      contracts i1 with i2, i3 with i4, ... (1-based)
%
%      optional 3rd argument to specify rank if singleton
%      dimensions exist.
%
%  AW (C) 2006
