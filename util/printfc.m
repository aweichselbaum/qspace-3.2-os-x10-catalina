%  Usage: printfc(fmt)
%
%     C++ program that emulates C printf
%     ie. does not terminated automatically with a newline and
%     allows for escape sequences (in contrast to fprintf).
%
%     NB! If more than one onput argument is specified, all
%     arguments are forwarded to matlab's sprintf.
%
%  AWb (C) Jan 2006
