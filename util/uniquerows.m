%  Usage: [A [,I,D]] = uniquerows(Q [,opts])
%
%     MEX file that makes rows of array unique. the second return
%     argument is a cell array of index sets of rows with the same
%     data. D specifies the corresponding multiplicity of the
%     unique rows.
%
%  Options
%
%    '-1'  return only first index in each group in I
%
%  Wb (C) Sep 2006
