%  Usage: [E,U] = wbeig(H);
%
%      eigenvalue decomposition based on BLAS dsyev
%      for (genearlized) symmetric rank-2 object.
%
%  AW (C) 2009
