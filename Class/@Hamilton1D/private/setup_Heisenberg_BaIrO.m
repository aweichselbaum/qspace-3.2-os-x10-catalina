function [HAM]=setup_Heisenberg_BaIrO(varargin)
% function [HAM]=setup_Heisenberg_BaIrO([L, opts])
%
%    setup 2-leg brick-ladder with frustration term
%    see Ba4Ir3O10_structure_model.pdf
%
%    NB! the setup can be mapped to 3-site periodic 2-leg system
%    where a rung is made up of 3 sites.
%
%    Using isotropic Heisenberg couplings:
%      J(1)   coupling along legs
%      J(2)   NN rung coupling with center site
%      J(3)   NNN rung-coupling for frustration, J2 ~ J3 ~ 0.2*J1)
%      J(4)   introduce center site as 3rd leg [default 0]
%   
% Wb,Jun15,20

% adapted from setup_HeisenbergTriLadder.m

  if nargin<1
     helpthis, if nargin || nargout, wberr('invalid usage'), end
     return
  end

  getopt('init',varargin);
     sym  = getopt('sym','SU2');
     qloc = getopt('qloc', 1);
     J    = getopt('J', [1, 0.2, 0.2]);
     dJ2  = getopt('dJ2',0);
     alpha= getopt('alpha', []);
     L    = getopt('L', 0);
     Dz   = getopt('Dz', 1); % motivated by Weiguo's Ising analysis
     perBC= getopt('--perBC');

     if getopt('-X'); xflag=1;
     elseif getopt('--Xzz'); xflag=2;
     else xflag=0; end

     tflag= getopt('-t');

     Aflag= getopt('-A');

  if L<=0, L=getopt('get_last',[]);
  else getopt('check_error'); end

  s=size(J);
  if s(1)~=1 || s(2)>4
     wberr('invalid J data (non-uniform system; %g x %g)',s); end
  J(:,end+1:4)=0;
  gotJ4=norm(J(:,4));
  if gotJ4, wblog('NB!','using 3-leg ladder with Jc=%g',J(4)); end

  if ~isempty(alpha)
     if L<=0, L=numel(alpha); if L>3, wblog(' * ',...
        'got implicit length specification (L=%g)',L);
     end, end
     if ~isvector(alpha) || numel(alpha)~=L, whos alpha
        wberr('invalid usage (invalid alpha; L=%g)',L);
     end
     q=unique(alpha); if numel(q)==1
        wblog(' * ','merging alpha with J(2:3)'); 
        J(2:3) = J(2:3) + (alpha/2)*[1,-1]; alpha=[];
     end
  end

  if L<3, wberr('invalid usage (L=%g)',L); end

  if ~Aflag && Dz~=1, Aflag=2; end

  istr=mfilename;
  param=add2struct('-',istr,L,J,Dz,dJ2,alpha,sym,qloc,Aflag,xflag,perBC);

  if isempty(alpha),  alpha=0;
  else aJ=alpha*J(1); alpha=1; end

  [Sleg,Hloc,Xdif,Eloc,Sloc,I1,param] = get_ops_BrickLadder(sym,param);

  if Aflag, param.sym=getsym(Hloc); end

  dloc=getDimQS(Eloc); dloc=dloc(:,1);
  s=I1.istr; if ~isempty(s), s=[s ' with ']; end
  s=['using ' s 'supersite @ '];
  if numel(dloc)>1
       s=[s 'd^\ast=' sprintf('%g (%g)',dloc)];
  else s=[s 'd='      sprintf('%g',     dloc)];
  end

  if alpha
       jstr=sprintf(', alpha=[%.3g .. %.3g]',[min(aJ),max(aJ)]/J(1));
  else jstr=''; end
  if ~gotJ4
       jstr=sprintf('J=[%g %g; %g]%s',J(1:3),jstr);
  else jstr=sprintf('J=[%g %g; %g; %g]%s',J, jstr);
  end

  HAM=struct(Hamilton1D);
  HAM.info.istr=['Heisenberg BaIrO ladder ' jstr ' ' s];
  HAM.info.param=param;
  HAM.info.IS=I1;

  HAM.info.lops=Sloc;

  if Aflag
     q=split_ops(Sloc);
     HAM.info.xops=fixScalarOp(q(:,2));
  else
     HAM.info.xops=Sloc;
  end

  n=numel(Sloc);
  for i=1:n, for j=i+1:n
     HAM.info.xops(end+1)=contract(Sloc(i),'13*',Sloc(j),'13');
  end, end

  HAM.oez=init_ops(Eloc,'local identity operator (E3)');

  HAM.ops=[
     init_ops(Hloc,'H3_rung','~hconj')
     init_ops(Sleg,'(S.S)_legs','~hconj')
  ];

  if alpha
     HAM.ops(end+1)=init_ops(Xdif,'H3_Xdif','~hconj');
  end

  stype=[]; l=1; HH=zeros(2*L-1,5);

  if ~perBC
     for i=1:L
        HH(l,:)=[ [i, 1 ],  [i,   1    ], J(2) ]; l=l+1; if alpha
        HH(l,:)=[ [i, 3 ],  [i,   3    ], aJ(i)]; l=l+1; end; if i<L
        HH(l,:)=[ [i, 2 ],  [i+1, 2    ], J(1) ]; l=l+1; end
     end
  else
     wblog('NB!','using periodic BC (interleaved setup)'); 

     XY=[ (1:L)', zeros(L,1) ]; XY(2:2:end,2)=1;
     HAM.info.XY=XY;

     for i=1:L
        HH(l,:)=[ [i,   1  ], [i,   1  ], J(2) ]; l=l+1; if alpha
        HH(l,:)=[ [i,   3  ], [i,   3  ], aJ(i)]; l=l+1; end
        HH(l,:)=[ [i-1, 2  ], [i+1, 2  ], J(1) ]; l=l+1;
     end

     HH(2+alpha,1)=1;
     HH(end,3)=L;
  end

  HAM=setup_mpo(HAM,HH,stype);
  HAM.store='DMRG_BaIrO';

  if tflag, plot(HAM), end

end

% -------------------------------------------------------------------- %
% NB! build super-site to permit NN real-time evolution
% NB! the 6-site unit cell of the 2-leg ladder ...
%                                   
%     ┌───────────┐───────────┐───────
%     │ (1)---(6)-│-(7)---(C)-│-(D)-..
%     │  |        │  |        │  |
%     │ (2)       │ (8)       │ (E)
%     │  |        │  |        │  |
%     │ (3)---(4)-│-(9)---(A)-│-(F)-..
%     │        |  │        |  │
%     │       (5) │       (B) │
%     │        |  │        |  │
%     │ [1]...[6].│.[7]...[C].│.[D]...
%     └───────────┘───────────┘───────
%      SITE_1      SITE_2      SITE_3  
%
% can be equivalently redrawn as 2-leg ladder with 3-site periodicity (!) ...
%
%     ┌─────┐─────┐─────┐─────┐──────
%   a │ (1)-│-(6)-│-(7)-│-(C)-│-(D)..
%     │  |  │  |  │  |  │  |  │  |
%   b │ (2) │ (5) │ (8) │ (B) │ (E)        tags: b_center
%     │  |  │  |  │  |  │  |  │  |
%   c │ (3)-│-(4)-│-(9)-│-(A)-│-(F)..
%     └─────┘─────┘─────┘─────┘──────
%       s1    s2    s3    s4    s5 ..
%
% Wb,Jun15,20
% -------------------------------------------------------------------- %

function [Sleg,Hloc,Xdif,Eloc,Sloc,I1,p]=get_ops_BrickLadder(sym,p)

  if numel(p.J)~=4
     error('Wb:ERR','invalid usage');
  end
  istr={};

  if p.Aflag, p.sym='U1';
     [S1,I1]=getLocalSpace('Spin',p.qloc/2,'-A','-v');
     if p.Dz~=1, i=1;
        if p.Dz<0, wberr('invalid Dz=%g',p.Dz); end
        if numel(S1)~=3 || numel(S1(i).Q)~=3 || norm(S1(i).Q{3})
           wberr('unexpected S1 operators');
        end
        S1(i)=sqrt(p.Dz)*S1(i);
        istr{end+1}=sprintf('Dz=%g',p.Dz);
     end
     S1=sum(S1);
  else
     [S1,I1]=getLocalSpace('Spin',p.sym,p.qloc,'-v');
  end
  E1=I1.E;

  j2=p.dJ2/p.J(2);
  if j2
     wblog('-->','using dJ2=%g',p.dJ2); 
     istr{end+1}=sprintf('dJ2=%g',p.dJ2);
  end

  j3=p.J(3)/p.J(2);  % 100.3/100 -> '1.003' - ok.
  if j3~=1, istr{end+1}=['j3=',num2rat(j3)]; end

  j4=p.J(4)/p.J(1);
  if j4, istr{end+1}=['jc=',num2rat(j4)]; end

  if isempty(istr), I1.istr='';
  else s=sprintf(', %s',istr{:}); I1.istr=s(3:end); end

  A2=getIdentityQS(E1,E1);
  A3=getIdentityQS(A2,3,E1,1);
  A3=contractQS(A2,3,A3,1);

  X=contractQS(A3,'321*',A3,'123');
  [pp,IX]=eigQS(X); 
  Ux=IX.AK; Ux.data{1}=fliplr(Ux.data{1});
  A3=setitags(QSpace(contractQS(A3,4,Ux,1)),{'a','b','c','abc'});

  I1.Ux=Ux;
  I1.A3=A3; X=untag(A3); 
  I1.X3=contract(X,'321*',X,'123');

  SS=[ contract(A3,'!4*',{S1,'-op:a','*',{S1,'-op:b',A3}})
       contract(A3,'!4*',{S1,'-op:b','*',{S1,'-op:c',A3}})
       contract(A3,'!4*',{S1,'-op:a','*',{S1,'-op:c',A3}}) ];

  Hloc=(1+j2)*SS(1)+(1-j2)*SS(2)+j3*SS(3);
  Xdif=SS(1)+SS(2)-   SS(3);

  Sa=contract(A3,'!4*',{A3,S1,'-op:a'});
  Sb=contract(A3,'!4*',{A3,S1,'-op:b'});
  Sc=contract(A3,'!4*',{A3,S1,'-op:c'}); Sloc=[Sa Sb Sc];

  A6=setitags(QSpace(getIdentityQS(A3,4,A3,4)),{'s1','s2','s12'});
  H6 = contract(A6,'!3*',{Sa,'-op:s1','*',{Sa,'-op:s2',A6}}) ...
     + contract(A6,'!3*',{Sc,'-op:s1','*',{Sc,'-op:s2',A6}});
  if j4, H6 = H6 ...
     + j4*contract(A6,'!3*',{Sb,'-op:s1','*',{Sb,'-op:s2',A6}});
  end

  H4=QSpace(contractQS(A6,'!12',{H6,A6,'*'}));
  [Sleg,Sl_]=splitH4_SdotS(H4,'-v');

  e=norm(Sleg-Sl_);
  if e>1E-12, error('Wb:ERR','unexpected setting (e=%g)',e); end

  Eloc=getIdentity(A3,4);
  untag(Sleg,Hloc,Eloc,Sloc);

  if p.xflag, q=iff(all(p.J(:,2:3)>=2),'==>','WRN');
     wblog(q,'projecting out S=3/2 multiplet space');

     project_out_q3(Sleg,Hloc,Xdif,Eloc,Sloc);

     if p.xflag>1
     end
  end
end

% -------------------------------------------------------------------- %
function project_out_q3(varargin)

   for l=1:nargin
      X=varargin{l}; n=inputname(l); 
      if isempty(n), error('Wb:ERR','invalid usage'); end
      for j=1:numel(X)
         q=[X(j).Q{1}, X(j).Q{2}]; i=find(sum(q<=1,2)==2);
         X(j)=getsub(X(j),i);
      end
      assignin('caller',n,X);
   end

end

% -------------------------------------------------------------------- %

