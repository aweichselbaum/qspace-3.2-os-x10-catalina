function i=eq(A,B)
% overload of == (equal) operator

  i=isequal(A,B);

end

