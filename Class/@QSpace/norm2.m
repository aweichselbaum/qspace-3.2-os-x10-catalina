function M=norm2(A)
% wrapper routine norm2 -> normQS()
% Wb,Oct18,14

  M=norm(A).^2;

end

