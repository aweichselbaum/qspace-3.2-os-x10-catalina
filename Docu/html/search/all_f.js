var searchData=
[
  ['sbuf_0',['SBUF',['../classwblog_1_1_s_b_u_f.html',1,'wblog']]],
  ['sparsecollector2d_1',['sparseCollector2D',['../classsparse_collector2_d.html',1,'']]],
  ['sparseindex2d_2',['sparseIndex2D',['../classsparse_index2_d.html',1,'']]],
  ['spectral_3',['Spectral',['../class_spectral.html',1,'']]],
  ['sptr_4',['sptr',['../class_wb_1_1sptr.html',1,'Wb']]],
  ['sptr_3c_20double_20_3e_5',['sptr&lt; double &gt;',['../class_wb_1_1sptr.html',1,'Wb']]],
  ['sptr_3c_20int_20_3e_6',['sptr&lt; int &gt;',['../class_wb_1_1sptr.html',1,'Wb']]],
  ['sptr_3c_20td_20_3e_7',['sptr&lt; TD &gt;',['../class_wb_1_1sptr.html',1,'Wb']]],
  ['sptr_3c_20ts_20_3e_8',['sptr&lt; TS &gt;',['../class_wb_1_1sptr.html',1,'Wb']]],
  ['stat_5fdgemm_9',['STAT_DGEMM',['../class_s_t_a_t___d_g_e_m_m.html',1,'']]],
  ['stdio_5fbuf_10',['stdio_buf',['../classwblog_1_1stdio__buf.html',1,'wblog']]],
  ['string_11',['String',['../class_wb_1_1_string.html',1,'Wb']]],
  ['svddata_12',['SVDData',['../class_s_v_d_data.html',1,'']]],
  ['symmetry_13',['Symmetry',['../class_d_y_1_1_symmetry.html',1,'DY']]]
];
